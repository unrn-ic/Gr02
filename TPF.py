
#calculadora clasica
#funciones
def mostrar_resultado(resultado):
    "Permite mostrar el resultado solo si se ingresa el ="
        
    igual = input("ingrese = para ver el resultado")
    if igual == "=":
        print(f"El resultado es: {resultado}" )
    else:
        while igual != "=":
            igual = input("ingrese = para ver el resultado")            
    return()
def suma(lista):
    resultado =lista[0]
    for i in range(1,len(lista)):
        resultado+=lista[i]
    mostrar_resultado(resultado)
    
    return()

def resta(lista):
    resultado =lista[0]
    for i in range(1,len(lista)):
        resultado-=lista[i]
    mostrar_resultado(resultado)
    
    return()

def mult(lista):
    resultado =lista[0]
    for i in range(1,len(lista)):
        resultado*=lista[i]
    mostrar_resultado(resultado)

    return()

def div(lista):
    resultado =lista[0]
    for i in range(1,len(lista)):
        resultado/=lista[i]
    mostrar_resultado(resultado)

    return()

def clasica():
    lista=[]
    i=True
    while i==True:
        num=float(input("ingrese un numero o escriba 0 para terminar: \n"))
        if num!=0:
            lista.append(num)
        else:
            select_m = input(f"elija la operacion: \n- suma (1)\n- resta(2) \n- multiplicacion(3)\n- division(4)\n- atras (0)\n")
            if select_m=="1":
                suma(lista)
                
            elif select_m=="2":
                resta(lista)
                
            elif select_m=="3":
                mult(lista)
                
            elif select_m=="4":
                div(lista)
                
            elif select_m=="0":
                i=False
            else:
                print ("valor invalido")
    


    
# Fracciones
def calcu_fracciones():
    #Funciones
    def ingresar_fraccion():
        "Permite ingresar una fraccion, pidiendo numerador y denominador por separado"
        numerador = int(input("Ingrese el numerador: "))
        denominador = int(input("Ingrese el denominador: "))
        return numerador, denominador
   
    def mcd(a, b):
        """ Calcula el maximo comun divisor entre a y b usando el algorimo de euclides"""
        while b != 0:
            guardar = b
            b = a % b
            a = guardar
        return a

    def mcm(a, b):
        """ Calcula el minimo comun multiplo de a y b, basandose en que a*b=mcm(a,b)*mcd(a,b)"""
    
        return a * b // mcd(a, b)
    def simplificar_fraccion(fraccion):
        "Simplifica la fraccion dividiendo numerador y denominador por el mcd de ambos"
        numerador, denominador = fraccion
        mcd_numerador_denominador = mcd(numerador, denominador)
        return numerador // mcd_numerador_denominador, denominador // mcd_numerador_denominador


    def sumar_fracciones(fracciones):
        "Suma fracciones y devuelve el resultado simplificado"
        denominador_comun = 1
        for fraccion in fracciones:
            denominador_comun = mcm(denominador_comun, fraccion[1])

        numerador_suma = 0
        for fraccion in fracciones:
            numerador_suma += fraccion[0] * (denominador_comun // fraccion[1])

        return simplificar_fraccion((numerador_suma, denominador_comun))

    def restar_fracciones(fracciones):
        "Resta fracciones y devuelve el resultado simplificado"
        denominador_comun = 1
        for fraccion in fracciones:
            denominador_comun = mcm(denominador_comun, fraccion[1])

        numerador_resta = fracciones[0][0] * (denominador_comun // fracciones[0][1])
        for i in range(1, len(fracciones)):
            numerador_resta -= fracciones[i][0] * (denominador_comun // fracciones[i][1])

        return simplificar_fraccion((numerador_resta, denominador_comun))

    def multiplicar_fracciones(fracciones):
        "Multiplica fracciones y devuelve el resultado simplificado"
        numerador_multi = 1
        denominador_multi = 1
        for fraccion in fracciones:
            numerador_multi *= fraccion[0]
            denominador_multi *= fraccion[1]

        return simplificar_fraccion((numerador_multi, denominador_multi))

    def dividir_fracciones(fracciones):
        "Divide fracciones y devuelve el resultado simplicado"
        numerador_div = fracciones[0][0]
        denominador_div = fracciones[0][1]
        for i in range(1, len(fracciones)):
            numerador_div *= fracciones[i][1]
            denominador_div *= fracciones[i][0]

        return simplificar_fraccion((numerador_div, denominador_div))

    def mostrar_fraccion(fraccion):
        "Permite mostrar la fraccion solo si se ingresa el ="
        numerador, denominador = fraccion
        igual = input("ingrese = para ver el resultado")
        if igual == "=":
            print(f"El resultado es: {numerador}/{denominador}")
        else:
            while igual != "=":
                igual = input("ingrese = para ver el resultado")
            print(f"El resultado es: {numerador}/{denominador}")

                
    # Programa principal
    i = 0
    while i == 0:
        print("Operaciones con fracciones")
        print("1. Sumar")
        print("2. Restar")
        print("3. Multiplicar")
        print("4. Dividir")
        print("0. Atras")

        opcion = int(input("Ingrese el número correspondiente a la operación que desea realizar: "))
        if opcion == 0:
            i =1
        else:
            num_fracciones = int(input("Ingrese la cantidad de fracciones a calcular: "))

            fracciones = []
            for i in range(num_fracciones):
                print(f"Ingrese los datos de la fracción {i + 1}:")
                fraccion = ingresar_fraccion()
                fracciones.append(fraccion)

            if opcion == 1:
                resultado = sumar_fracciones(fracciones)
                mostrar_fraccion(resultado)
            elif opcion == 2:
                resultado = restar_fracciones(fracciones)
                mostrar_fraccion(resultado)
            elif opcion == 3:
                resultado = multiplicar_fracciones(fracciones)
                mostrar_fraccion(resultado)
            elif opcion == 4:
                resultado = dividir_fracciones(fracciones)
                mostrar_fraccion(resultado)
            i = 1
        
#Calculadora de conversiones
def decimal_a_binario(decimal):
    binario = ""

    if decimal >= 1:
        while decimal > 0:
            resto = decimal % 2
            decimal = decimal // 2
            binario = str(resto) + binario

    return binario

def decimal_a_hexadecimal(decimal):
    hexadecimal = ""

    while decimal > 0:
        residuo = decimal % 16
        if residuo < 10:
            hexadecimal = str(residuo) + hexadecimal
        else:
            hexadecimal = chr(ord('A') + residuo - 10) + hexadecimal
        decimal = decimal // 16

    return hexadecimal

def decimal_a_octal(decimal):
    octal = ""

    while decimal > 7:
        resto = decimal % 8
        octal = str(resto) + octal
        decimal = decimal // 8
    octal = str(decimal) + octal

    return octal

def conversion():
    decimal = int(input("Ingrese un número: \n"))
    if decimal < 0 or decimal > 9999:
        print("Valor incorrecto")
        return
    
    select_m = input(f"Elija la operación: \n- Binario (1)\n- Hexadecimal (2) \n- Octal (3)\n")
    if select_m == "1":
        binario = decimal_a_binario(decimal)
        print(f"{decimal} en binario es: {binario}")
    elif select_m == "2":
        hexadecimal = decimal_a_hexadecimal(decimal)
        print(f"{decimal} en hexadecimal es: {hexadecimal}")
    elif select_m == "3":
        octal = decimal_a_octal(decimal)
        print(f"{decimal} en octal es: {octal}")
    else:
        print("Valor incorrecto")

    
#main

while True:
    on=input(f"Escriba On para encender la calculadora: \n")
    if on.lower()=="on":
        print ("Bienvenidos a la calculadora: ")
        while True:
            select_m = input(f"seleccione la calculadora que va a  utilizar: \n- calculadora clasica (1)\n- calculadora de fracciones(2) \n- calculadora de conversiones(3)\n- salir(Off)\n")
            if select_m=="1":
                clasica()
            elif select_m=="2":
                calcu_fracciones()
            elif select_m=="3":
                conversion()
            elif select_m=="off":
                break
            else:
                print ("valor invalido")
